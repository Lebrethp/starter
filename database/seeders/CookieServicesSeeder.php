<?php

namespace Database\Seeders;

use App\Models\Cookies\CookieService;
use Illuminate\Database\Seeder;

// Todo: update this seeder if your app is not multilingual.

class CookieServicesSeeder extends Seeder
{
    public function run(): void
    {
        CookieService::factory()->withCategories(['necessary'])->create([
            'unique_key' => 'session',
            'title' => ['fr' => 'Session', 'en' => 'Session'],
            'description' => ['fr' => null, 'en' => null],
            'required' => true,
            'enabled_by_default' => true,
            'cookies' => null,
            'active' => true,
        ]);
        CookieService::factory()->withCategories(['security'])->create([
            'unique_key' => 'csrf_token',
            'title' => ['fr' => 'Clé CSRF', 'en' => 'CSRF Token'],
            'description' => ['fr' => null, 'en' => null],
            'required' => true,
            'enabled_by_default' => true,
            'cookies' => null,
            'active' => true,
        ]);
        CookieService::factory()->withCategories(['statistic'])->create([
            'unique_key' => 'matomo',
            'title' => ['fr' => 'Matomo', 'en' => 'Matomo'],
            'description' => ['fr' => null, 'en' => null],
            'required' => false,
            'enabled_by_default' => true,
            'cookies' => [
                '^_pk_ref.*$',
                '^_pk_cvar.*$',
                '^_pk_id.*$',
                '^_pk_ses.*$',
                '^mtm_consent.*$',
                '^mtm_consent_removed.*$',
                '^mtm_cookie_consent.*$',
                '^matomo_ignore.*$',
                '^matomo_sessid.*$',
                '^_pk_hsr.*$',
                '^_pk_uid.*$',
            ],
            'active' => true,
        ]);
        CookieService::factory()->withCategories(['advertising'])->create([
            'unique_key' => 'twitter_feed',
            'title' => ['fr' => 'Fil Twitter intégré', 'en' => 'Twitter Embed Feed'],
            'description' => ['fr' => null, 'en' => null],
            'required' => false,
            'enabled_by_default' => false,
            'cookies' => [
                'ct0',
                '_twitter_sess',
                'guest_id',
                'personalization_id',
                'external_reference',
                'eu_cn',
                'kdt',
                'lang',
                'remember_check_on',
                'tfw_ex',
            ],
            'active' => false,
        ]);
        CookieService::factory()->withCategories(['advertising'])->create([
            'unique_key' => 'youtube_video',
            'title' => ['fr' => 'Vidéos Youtube intégrées', 'en' => 'Youtube Embed Videos'],
            'description' => ['fr' => null, 'en' => null],
            'required' => false,
            'enabled_by_default' => false,
            'cookies' => ['GEUP', 'PREF', 'VISITOR_INFO1_LIVE', 'YSC'],
            'active' => false,
        ]);
    }
}
